---
title: "Donate"
---
<p> We run services powered by Free Software which respect user privacy for the general public. These services are maintained by our volunteers in their free time. We rely on donations by people like you to cover the costs (cost of renting servers, buying domain name etc.) of running these services. To contribute financially to our services, please transfer the amount in the below mentioned account. After you donate and fill the form below, your name will be added to the <a href="/contributors">contributors list</a>. It is manually updated by volunteers, so it might take some time for your name to show up. If you have donated and filled the form below, you can also remind us to add you in the contributors list by sending an email to contact at fsci dot in. </p>

<div class="cta" style="margin:auto; width:250px">
<a href="https://rzp.io/l/Dq4nDsSDhe"><button>Donate using card</button></a>
</div>

If you prefer not using a card, please transfer the amount in the following account:

<br>
<div class="accdetails">
<h2>Account Details</h2>

 <strong>Name:</strong> Sruthi Chandran
 
 <strong>Account No:</strong> 104501511006
 
 <strong>IFSC Code:</strong> ICIC0001045

 <p>If you prefer UPI, the details are below:</p>
 
 <strong>UPI ID:</strong> srud@icici
</div>


<div class="accdetails">
<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-alert-triangle" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
   <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
   <path d="M12 9v2m0 4v.01"></path>
   <path d="M5 19h14a2 2 0 0 0 1.84 -2.75l-7.1 -12.25a2 2 0 0 0 -3.5 0l-7.1 12.25a2 2 0 0 0 1.75 2.75"></path>
</svg>

Please do not forget to fill the following form with name of the service you want to support and transaction details. This will be used to identify which service will receive your payment.
</div>

<div style="position: relative; height:100vh; max-height:100vh; overflow:auto;"> 
    <iframe 
    src="https://app.formbricks.com/s/clpf5f2103pfgdl2i3p1hs4g0" 
    frameborder="0" style="position: absolute; left:0; top:0; width:100%; height:100%; border:0;">
    </iframe>
</div>

