---
title: "Contact Us"
---

## Contact Us

Volunteers can contribute to FSCI in various ways, listed below:

Technical contributions:

- Programming/Coding
- System Administration for the different services we run
- Debian Packaging of services that use native packages. Many services we run uses native debian packages, so contributing to maintaining these packages help us run our services well.
- [Provide technical support](/tech-support) in our online groups when people ask for help.

Non-technical contributions:

- Contribute by writing articles campaigning for free software on our website.
- Monitor funding status of various services and organize crowd funding campaigns when funds are depleted.
- Raise awareness about free software, privacy etc.
- Organizing events and talks.
- By designing posters, banners etc.

Financial contributions:

- By [donating](https://fsci.in/donate) to [services](/#poddery) run by FSCI.

To contact or volunteer for us, please fill the contact form below.

<div style="position: relative; height:100vh; max-height:100vh; overflow:auto;"> 
    <iframe 
    src="https://app.formbricks.com/s/cloo46uhq4kgfnz0forvi5753" 
    frameborder="0" style="position: absolute; left:0; top:0; width:100%; height:100%; border:0;">
    </iframe>
</div>

For any queries, please send emails to [ⒸⓄⓃⓉⒶⒸⓉ@ⒻⓈⒸⒾ.ⒾⓃ](mailto:) If you need technical support related to free software, we would be happy to help you. Feel free to drop by at our [Tech Support group](/tech-support).

You can also reach out to us via matrix, xmpp or IRC, mentioned in the footer of this page.
