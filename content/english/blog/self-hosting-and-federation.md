---
title: "Self Hosting, Federation and end-to-end encryption - Freedom and privacy in the 'cloud'"
draft: false
date: 2021-09-27T18:18:45+05:30
discussionlink: https://codema.in/d/PmxpQk3w/self-hosting-and-federation-freedom-in-the-cloud-
---

Now-a-days, a lot of apps that we run are not even installed in our own computers. When you visit Google Docs through a browser, the software Google Docs is not installed in your computer, rather it is running on someone else's computers (in this case, Google). The computer you are running the software from is called a server. Since you are using a remote computer for your computing, you lose control over your own computing. Google Docs is only an example. A lot of software we use in our daily lives run on servers. Some examples are: Zoom, WhatsApp, Facebook, Conversations etc. Some of them have an app as well as servers, like Zoom has an app which uses its servers to communicate. Note that servers are an integral part of the working of these apps.

How do we control a software running on a server? What does user freedom mean in such a scenario?

If the software is free software, then we control the software. In addition to being a free software, if we control the servers our software are running on, then we can say that we control our computing. We can run our own server and have total control over it. This practice is known as self-hosting.

Self-hosting does have drawbacks:

- It needs a lot of skill and so it is not accessible to non-technical people.

- It is a very time and resource-consuming activity.

Companies like Google are running servers which take a lot of skill to set up, effort to maintain and costs a lot of money. They are funded by your data. They usually record your whole lives or get significant amount of personal data from you. Most of the software in use is proprietary and therefore all this data collection amounts to recording your whole life. This has [chilling effects on the society](https://socialcooling.com).

We campaign for freedom and privacy to all the users. Therefore, we ask the question: how would everyone get privacy and control of the software in the world where we are using remote servers for our computing? [YunoHost](https://yunohost.org) and [FreedomBox](https://freedombox.org/) make self hosting easier for people.

We can easily say that everyone should self-host but that is not possible. It requires a lot of skill, time and resources. Even people who can self-host need to invest a lot of time in maintaining these servers.

One way is to pay someone to set up a server for you. You get everything in your control as per your own requirements. Examples are [Matrix Services](https://element.io/matrix-services), [Deeproot GNU/Linux](https://deeproot.in/), [masto.host](https://masto.host/) etc.

Another way is running these services and maintain them as a community. People can volunteer to run these services in free time. Some people can do technical tasks, other people can do admin work, manage funds, organize crowdfunding, design posters, etc. We, as a community, can run these services we would like to and this way not everyone needs to self-host but gets the benefit of it. FSCI itself runs a lot of services, thanks to volunteers. People who cannot invest their time in volunteering can contribute by donating money.

The community-aspect of free software is also prominent in software installed on our computers. Consider the examples of free software like Debian(or pretty much any GNU/Linux distro) or Libreoffice. We trust the Debian community for the software to work well and respect the users. Unlike Microsoft Windows and macOS, this trust is not a blind trust. In Debian or any GNU/Linux distro, we can inspect the source code and adapt it as well. We can remove the malfunctionalities from the software and then share with everyone so that they also benefit. Nobody develops the whole operating system on their own and each software they use. Nor can everyone inspect every source-code (but it should be available). So, we rely on community-run software for our computing. Similarly, we can have community-run services.

Therefore, we can rely on free software powered community-hosted services. These services also vary in their nature and deployment and raise some ethical issues. For example, the service must be self-hostable and accessible using free software. Self-hosting is enough for some services like videoconferencing and search engines but it is not enough for chatting systems(system means combination of app and server), social media, email etc. In these systems, we need federation. Think of federation as users registered on different servers can communicate with each other. Email is an example of federation-- users having @gmail accounts can send emails to users on @yahoo.com email. Examples of federation are: email, XMPP, Matrix, Mastodon etc. There are examples of free software that do not allow you to self-host, like Telegram. Signal is an example of free software which we can self-host but not federated.

Let's illustrate the example of Matrix which is a chat protocol. A user registered at matrix.org can communicate with a user registered with poddery.com. The user can use free software (like Element) + community-maintained server to ensure their control over the software. Yes, this model relies on trusting communities and servers to an extent but the other side, namely, proprietary software and proprietary services are not even trustworthy. We, at FSCI, use matrix and XMPP for our communications. Most of the Matrix and XMPP apps also use end-to-end encryption in chats by default which means only people exchanging the messages can read the messages.

Etherpad and Cryptpad are examples of collaborative editing software similar to Google Docs. The difference being Cryptpad and Etherpad are free software and can be installed in servers controlled by us. But Google Docs is nonfree and cannot be installed in our own servers.

Notice how privacy is an even more complicated issue. Let's say you are running your own email server and you have everything under your control. So, we will say that you control the whole infrastructure of your email system. You can decide which data you would like to log and retain. But as soon as you send an email to a Gmail user, Google knows about it and your privacy is violated as Google is a surveillance company. If you don't want Google or anyone other than your friend to read your mails, then use can use [pep app](/blog/pep-guide) which encrypts emails automatically. Even if you encrypt, Google will get the metadata of your emails whenever you send emails to Gmail users. This scenario means that just using free software and controlling our own servers is not enough to protect our privacy. The attack is on freedom as well because if every user uses WhatsApp, your switch to any chat app is not useful at all. Therefore, we need activism more than ever. In the earlier days of computing, running free software in our own computers was enough to protect our liberty. But now, we need more. We need to be activists and spread free software and community-powered services and help people in switching to free software powered services.

To contribute to free software, you can volunteer for FSCI by filling the form available at our [contact page](/contact).
