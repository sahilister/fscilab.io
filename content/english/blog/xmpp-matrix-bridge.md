---
title: "How to join Matrix rooms via XMPP and vice-versa?"
date: 2021-07-15
author: "false"
tags: ["xmpp", "matrix"]
draft: false
---

Matrix-XMPP bridge allows us to link Matrix rooms and XMPP rooms (called public channels) which means XMPP users can post messages in Matrix rooms (using XMPP itself) and Matrix users can post messages in XMPP rooms (using Matrix itself).
Note - Matrix-XMPP bridging support is available for unencrypted rooms only as of now.

To create an XMPP-Matrix bridged room, we will use [Conversations app](https://conversations.im/) for XMPP for demonstration. You can choose XMPP [client of your choice](https://xmpp.org/software/clients.html). We'll use [this documentation](https://github.com/matrix-org/matrix-bifrost/wiki/Address-syntax) for bridging.

Steps to create a bridged room:

- Create an XMPP room. For this, press the '+' button in the Conversations app (or XMPP app of choice) and select the option 'create public channel'.

<img src="/img/1.png" alt="Join Public Channel" width="200"/>

- Name the channel. As an example, we are naming the public channel as 'Test Channel'.

<img src="/img/2.jpg" alt="Give Channel a name" width="200"/>

- Note the XMPP ADDRESS. Go to 'channel details' and note the XMPP address of the room.

<img src="/img/3.jpg" alt="Note XMPP Address" width="200"/>

- The XMPP address will be of the form `MUCNAME@MUCDOMAIN`. In our example, `MUCNAME` is `test-channel` and `MUCDOMAIN` is `muc.pwned.life`.

<img src="/img/4.jpg" width="200"/>

- From the [link](https://github.com/matrix-org/matrix-bifrost/wiki/Address-syntax) , use the MUCs syntax from Matrix → XMPP section.

- Insert MUCNAME and MUCDOMAIN in the syntax in step 5 obtained in step 4. Our example address is `#xmpp_test-channel_muc.pwned.life:matrix.org`

- In Matrix(for example, using Element app), go to 'Add room' → Explore Public Rooms → Type the Matrix address of the room obtained in step 6 → Join.

- The Matrix room will be automatically bridged to XMPP, so XMPP users can now receive messages posted by Matrix users in the room and vice-versa.

**Note** - In this tutorial, we first created an XMPP room (a public channel on XMPP) and then joined this room via Matrix. The drawback of this approach is that there is no admin access on the Matrix side as of now.

**Not Recommended**: The process can also be done the other way round as well, creating a Matrix room first and then joining it via XMPP. The drawback of the approach is that XMPP users miss the messages sent by Matrix users in the room when they're offline. Due to this, we do not recommend creating the Matrix room first and then bridging it to XMPP. Additionally this keeps xmpp id of participants in public channels private, because by default matrix id in public groups are visible to all participants of matrix rooms but it is only visible to admins on XMPP public channels. However, if you would like to create a Matrix room first and then bridge it to XMPP, use the above steps and replace XMPP by Matrix and vice-versa. To convert the room's Matrix address to XMPP address, use this [documentation](https://github.com/matrix-org/matrix-bifrost/wiki/Address-syntax).
