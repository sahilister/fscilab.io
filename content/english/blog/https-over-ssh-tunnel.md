---
title: "Secure a nextcloud instance on Debian 7 Wheezy that can no longer get certificates via certbot"
date: 2024-01-14T22:20:31+05:30
author: "false"
draft: false
tags: ["https", "ssh", "tunnel", "vpn", "nextcloud", "caddy", "debian", "certbot"]
discussionlink: https://codema.in/d/TwJ6qpgH/secure-a-nextcloud-instance-on-debian-7-wheezy-that-can-longer-get-certificates-via-certbot
---
We were running a shared NextCloud instance at nextcloud.libreinfra.org for sometime on Debian GNU/Linux 7 Wheezy. It was setup on a shared hosting tool that is no longer maintained (Manu would know the details), upgrading the host was not possible. Most other services were migrated to other systems but this nextcloud service remained on this host. Sometime back certbot stopped working (likely because the openssl version is too old and it does not support newer crypto) and we could not continue using https, so this was left untouched for a long time.

Recently Manu brought back the service without https, but I was reluctant to enter my password on a plain http site. I found two ways to securely access this site.

First was to create an ssh tunnel to an lxc container on my laptop and then connect to the container from my laptop browser.

`(real-libreinfra)<---ssh-tunnel--->(lxc container)<----http---> (browser)`

I created a tunnel inside my lxc container using this command.

`# ssh -L 10.0.3.218:80:nextcloud.libreinfra.org:80 root@libreinfra.org`

10.0.3.218 is the ip address of the lxc container.

Then on my laptop I added this in my /etc/hosts file

`10.0.3.218 nextcloud.libreinfra.org`

So visiting http://nextcloud.libreinfra.org will connect to my container and through the secure ssh tunnel will provide me access to my nextcloud instance securely.

I confirmed the connection is actually going via my container using ngrep command.

`sudo ngrep -d lxcbr0 any port 80`

I also had to add `ReadEtcHosts=yes` in my `/etc/systemd/resolved.conf` so my changes to `/etc/hosts` will be honored. Additionally I also added an exception for nextcloud.libreinfra.org in DNS over HTTPS settings on my firefox.

I settled for this since setting up a reverse proxy using Caddy on a server was not working. Since NextCloud/apache is strongly tied to the domain name (trusted domains setting for php and virtual hosts setting for apache), I could not get that working on my first try.

Abhijth (bhe) was curious to know how it was done the next day, so I gave it another fresh chance and I got it working.

On my server also I had to switch to systemd-resolved and enable `ReadEtcHosts=yes` in my `/etc/systemd/resolved.conf`

I added this crucial line `header_up Host {upstream_hostport} in /etc/caddy/Caddyfile`


```
oraclevm.j4v4m4n.in {
# Set this path to your site's directory.
root * /var/www/html

# Reverse proxy to nextcloud.libreinfra.org
reverse_proxy nextcloud.libreinfra.org:8080 {
header_up Host {upstream_hostport}
}

# Or serve a PHP site through php-fpm:
# php_fastcgi localhost:9000
}
```

Tunnel was created for 8080 port as caddy will need exclusive access to port 80.

`sudo ssh -L 127.0.0.3:8080:nextcloud.libreinfra.org:80 root@libreinfra.org`

and /etc/hosts was updated accrdingly

`127.0.0.3 nextcloud.libreinfra.org`

This was run inside a screen session, so I can detach and logout from the server but the tunnel will still be running under screen.

Now once this was setup, I was able to reach to nextcloud login page successfully, but nextcloud refused to service the actual login page with an error.

`Add "oraclevm.j4v4m4n.in" as trusted domain in config/config.php`.

I like it very much when error messages are helpful like this. Once this was added, anyone could access NextCloud securely ovet https at https://oraclevm.j4v4m4n.in (https to my server and then ssh tunnel to nextcloud server).

Note 1: If this host was running only NextCloud, then probably running caddy directly on nextcloud.libreinfra.org would be another option. Though then you will have to configure `php-fpm` and reverse proxy that in caddy. Since this was centrally managed by vesta cp and apache, just configuring nextcloud only in caddy is probably more involved (may be another day I can try - I have to figure out where vesta is installing nextclound and then replace apache cgi with php-fpm and then configure caddy as reverse proxy). This was suggested by Akshay. I did try it but since port 80 was already managed by apache/vesta cp, I did not proceed further (since I already found two different methods working).

Note 2: Another option suggested by Akshay is pointing DNS entry of nextcloud.libreinfra.org to another host with more recent openssl, get certificates via certbot, copy it to this server and switch back the DNS entry.

Note 3: A less resource intensive method would be to use systemd on demand to create a tunnel like I have been using to [send emails over ssh](https://p5r.uk/blog/2017/smtp-via-ssh-tunnel.html). Though this server did not have systemd and ssh key based login was not working.
