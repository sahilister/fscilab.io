---
title: "FSCI's statement on banning of Free Software apps like Element and Briar"
date: 2023-05-06T20:44:19+05:30
author: FSCI
tags: ["privacy", "federation", "instant-messaging", "statement", "encryption",
"free-software", "matrix"]
discussionlink: https://codema.in/d/F91wT4sM/element-banning-how-should-fsci-react-to-it
draft: false
---
Update on 15 May 2023: <i>Several media outlets, rights groups and Free Software projects have published articles based on this writeup to spread the word. The ones we know about are- <a href="https://scroll.in/latest/1048703/lack-of-understanding-software-body-criticises-centres-move-to-ban-14-applications">Scroll.in</a>, <a href="https://internetfreedom.in/14-mobile-apps-banned/">Internet Freedom Foundation</a>, <a href="https://www.theregister.com/2023/05/09/india_messaging_apps_ban/">The Register</a>, <a href="https://www.medianama.com/2023/05/223-element-app-statement-blocked-jk/">MediaNama</a>, <a href="https://epaper.mvkashmir.com/epaper/edition/198/paper/page/3">Mountain Valley Kashmir (print version)</a>, <a href="https://tutanota.com/blog/posts/apps-banned-india">Tutanota</a> and <a href="https://pirates.org.in/statements/our-response-to-banning-on-14-apps-including-element-briar-and-threema/">Indian Pirates</a>. Internet Freedom Foundation has also filed RTI requests to obtain more information.</i>

<i>A <a href="https://sflc.in/defending-digital-rights-sflc-in-assists-in-challenging-ban-on-apps/">writ petition</a> has been filed on the matter by Praveen and Kiran on behalf of FSCI with the assistance of lawyers from SFLC.in in the Kerala High Court.</i>

As per media reports, 14 apps including Free Software ones like Element and
Briar are banned in India as of 3rd May <sup>[1]</sup>. As per reports the
reasoning behind the ban seems to be, “These apps do not have any
representatives in India and cannot be contacted for seeking information as
mandated by the Indian laws”. This statement indicates to us that there are gaps
in understanding on how federated services work (see [Notes](#notes) section
below for a detailed explanation).

There is a lack of clarity on the manner in which the ban will be implemented.
We assume that the applications will be de-listed from the app stores.

Element, the company behind Element app, has put out a statement <sup>[2]</sup>
explaining their position on the ban. We get to know that Indian authorities
have contacted them in the past to which they have responded constructively
which goes against observed reasoning for the ban. Element also had to know
about the ban from media reports since there was no communication informing them
of the ban.

> While Element never compromises end-to-end encryption or user privacy, we have
> been contacted by Indian authorities in the past and addressed them in a
> constructive fashion (typically responding same-day).

> As we understand it, Indian government officials claim to have approved the
> ban due to Element (and other apps) not having representatives in India.

> That is a bit of guesswork on our part, because we did not receive any prior
> notice of the decision; clarification from the Ministry of Electronics and
> Information Technology would be most welcome.

There seems to be a lack of understanding on part of the government on how these
P2P software as well as federated apps work. These applications have been
crucial for communication during disasters and are used regularly as
communication medium in workplaces.

The ban, we believe, will not serve the purpose as there are many anonymous
alternate apps that can be used by terror outfits to fill their purpose.

Federated, peer-to-peer, encrypted, Free Software apps/software like Element and
Briar, should be promoted. They are key to our national security as they provide
means to enable sovereign, private and secure communication to citizens of
India. Element has been embraced by Governments of France <sup>[3]</sup>,
Germany <sup>[4]</sup> and Sweden <sup>[5]</sup> which should be an example for
India.

### Notes:

Email is federated, has existed for a long time, and the logic which they
applied to banning Element would apply to K-9 Mail, a Free Software client app
for email, as well. Email service is provided by many service providers like
Google, Microsoft and many others who don't have any representatives in India.
Matrix, like email, is federated and is the protocol behind the service. Element
is just one of the matrix clients and matrix.org is just one of the matrix
service providers. Banning all instances, clients and implementations of matrix
is similar to banning all email service providers, email clients and whole email
infrastructure, which would be nearly impossible for the government and a new
service provider and/or client can come up very easily rather quickly.

Similar to how Matrix is federated, Briar is a P2P (peer-to-peer) app which
means it does not even have a service provider and users who use it need to be
online concurrently. Also it does not require internet connection and can be
used over a bluetooth or wireless connection. It is useful in emergency
situations like natural disasters where all other media for communications
become offline.

We had to share the archive links to all element.io website links since it's already
blocked by multiple ISPs in India.

### References:

1: https://indianexpress.com/article/india/mobile-apps-blocked-jammu-kashmir-terrorists-8585046/

2: https://archive.is/IDNoe

3: https://archive.is/n7Oha

4: https://archive.is/Nialr

5: https://archive.is/EtTUO

[1]: https://indianexpress.com/article/india/mobile-apps-blocked-jammu-kashmir-terrorists-8585046/
[2]: https://archive.is/IDNoe
[3]: https://archive.is/n7Oha
[4]: https://archive.is/Nialr
[5]: https://archive.is/EtTUO
